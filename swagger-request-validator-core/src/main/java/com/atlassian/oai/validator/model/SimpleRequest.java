package com.atlassian.oai.validator.model;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

/**
 * Simple immutable {@link Request} implementation.
 * <p>
 * New instances should be constructed with a {@link Builder}.
 */
public class SimpleRequest implements Request {

    private final String path;
    private final Method method;
    private final Optional<String> requestBody;
    private final Map<String, Collection<String>> queryParams;
    private final Map<String, Collection<String>> headers;

    private SimpleRequest(@Nonnull final Method method,
                          @Nonnull final String path,
                          @Nullable final String body,
                          @Nonnull final Map<String, Collection<String>> queryParams,
                          @Nonnull final Map<String, Collection<String>> headers) {
        this.method = requireNonNull(method, "A method is required");
        this.path = requireNonNull(path, "A request path is required");
        this.requestBody = Optional.ofNullable(body);
        this.queryParams = requireNonNull(queryParams);
        this.headers = headers;
    }

    @Nonnull
    @Override
    public String getPath() {
        return path;
    }

    @Nonnull
    @Override
    public Method getMethod() {
        return method;
    }

    @Nonnull
    @Override
    public Optional<String> getBody() {
        return requestBody;
    }

    @Override
    @Nonnull
    public Collection<String> getQueryParameterValues(final String name) {
        if (name == null || !queryParams.containsKey(name.toLowerCase())) {
            return emptyList();
        }

        return unmodifiableList(
                queryParams.get(name.toLowerCase()).stream().filter(Objects::nonNull).collect(toList())
        );
    }

    @Override
    @Nonnull
    public Collection<String> getQueryParameters() {
        return Collections.unmodifiableCollection(queryParams.keySet());
    }

    @Nonnull
    @Override
    public Map<String, Collection<String>> getHeaders() {
        return Collections.unmodifiableMap(headers);
    }

    @Nonnull
    @Override
    public Collection<String> getHeaderValues(final String name) {
        if (name == null || !headers.containsKey(name)) {
            return emptyList();
        }

        return unmodifiableList(
                headers.get(name).stream().filter(Objects::nonNull).collect(toList())
        );
    }

    /**
     * A builder for constructing new {@link SimpleRequest} instances.
     */
    public static class Builder {

        private String path;
        private Method method;
        private String body;
        private Multimap<String, String> queryParams = MultimapBuilder.hashKeys().arrayListValues().build();
        private Multimap<String, String> headers = MultimapBuilder.hashKeys().arrayListValues().build();

        public static Builder get(final String path) {
            return new Builder(Method.GET, path);
        }

        public static Builder put(final String path) {
            return new Builder(Method.PUT, path);
        }

        public static Builder post(final String path) {
            return new Builder(Method.POST, path);
        }

        public static Builder delete(final String path) {
            return new Builder(Method.DELETE, path);
        }

        public static Builder patch(final String path) {
            return new Builder(Method.POST, path);
        }

        public Builder(final Method method, final String path) {
            this.method = requireNonNull(method, "A method is required");
            this.path = requireNonNull(path, "A path is required");
        }

        public Builder withBody(final String body) {
            this.body = body;
            return this;
        }

        public Builder withHeader(final String name, final List<String> values) {
            if (values == null || values.isEmpty()) {
                headers.put(name, null);
            } else {
                headers.putAll(name, values);
            }
            return this;
        }

        public Builder withHeader(final String name, final String... values) {
            return withHeader(name, asList(values));
        }

        public Builder withQueryParam(final String name, final List<String> values) {
            if (values == null || values.isEmpty()) {
                queryParams.put(name, null);
            } else {
                queryParams.putAll(name.toLowerCase(), values);
            }
            return this;
        }

        public Builder withQueryParam(final String name, final String... values) {
            return withQueryParam(name, asList(values));
        }

        public SimpleRequest build() {
            final TreeMap<String, Collection<String>> caseInsensitiveHeaders = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
            caseInsensitiveHeaders.putAll(headers.asMap());

            return new SimpleRequest(method, path, body, queryParams.asMap(), caseInsensitiveHeaders);
        }
    }
}
